cd /home/ubuntu/jarvis-minifi/minifi/minifi-0.5.0/bin
./minifi.sh stop
rm -rf ../content_repository/* ../provenance_repository/* ../flowfile_repository/* ../state/local/* ../logs/*
./minifi.sh start

# No CPU limit passed
if [ $# != 1 ]; then
    exit 1
fi
   
ps -ax | grep [o]rg.apache.nifi.minifi.MiNiFi | cut -d' ' -f1 | xargs -t cpulimit -l $1 -p 
