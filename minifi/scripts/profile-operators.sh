#cd /home/ubuntu/jarvis-minifi/minifi/minifi-0.5.0/bin
cd /mnt/ram_disk/jarvis-minifi/minifi/minifi-0.5.0/bin
./minifi.sh stop
rm -rf ../content_repository/* ../provenance_repository/* ../flowfile_repository/* ../state/local/* ../logs/*
./minifi.sh start

# No CPU limit passed
if [ $# -eq 1 ]; then
    echo "Imposing CPU limit of ${1}"
    ps -ax | grep [o]rg.apache.nifi.minifi.MiNiFi | cut -d' ' -f1 | xargs -t cpulimit -l $1 -p & 
fi

echo "sleeping for 1.7m"   
sleep 0.7m
echo "sampling CPU usage"
cpustat -x -D -a -n 1 1 30
cd /mnt/ram_disk/jarvis-minifi/minifi/minifi-0.5.0/bin
sudo ./minifi.sh stop
cd ../logs/ 
echo "Parsing results"
python3.5 ../../scripts/profile-operators.py minifi-app.log
