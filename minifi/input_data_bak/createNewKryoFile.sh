#!/bin/bash
if [ $# != 1 ]; then
    echo "Give full path of input csv file argument to intify and convert to kryo"
    exit 1
fi


fullFileName=$1
baseFileName=$(basename "${fullFileName}")
cd /mnt/ram_disk/jarvis-minifi/minifi/input_data_bak/intified

# This is for intifying source cluster for groupby
echo "Intifying source cluster for groupby"
python3.5 intifyStringFields.py $fullFileName 2

#This is for intifying error code for filter
echo "Intifying error code for filter"
python3.5 intifyStringFields.py "${fullFileName}.intify" 10

fileIntifyFullPath="/mnt/ram_disk/jarvis-minifi/minifi/input_data_bak/intified/${baseFileName}.intify"
echo $fileIntifyFullPath
mv $fullFileName."intify"."intify" $fileIntifyFullPath

echo "Comparing intified and original file"
vimdiff $fileIntifyFullPath $fullFileName

#TODO: remove intified file with source cluster only

echo "Converting to kryo now"
cd /mnt/ram_disk/jarvis-minifi/minifi/input_data_bak/kryoConverter
fileKryoFullPath="/mnt/ram_disk/jarvis-minifi/minifi/input_data_bak/kryoData/${baseFileName}.intify.kryo"
echo $fileKryoFullPath

java -cp rxjavatest.jar:kryo-5.0.0-RC4.jar:objenesis-3.0.1.jar:minlog-1.3.1.jar Main $fileIntifyFullPath $fileKryoFullPath
rm /mnt/ram_disk/jarvis-minifi/minifi/operator-profile-input-data/*
cp $fileKryoFullPath "/mnt/ram_disk/jarvis-minifi/minifi/operator-profile-input-data/"
