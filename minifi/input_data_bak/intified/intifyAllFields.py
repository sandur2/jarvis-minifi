#!/usr/bin/py
import sys
import statistics

file_name=sys.argv[1]
line_count=0
fieldval_mapping={}
count_mapping={}
out_file=file_name+".allintify"
fp_out=open(out_file,"w")
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		split_line=line.split(",")

		# Iterate through each column
		for col_idx in range(0,len(split_line)):
			if col_idx not in fieldval_mapping:
				fieldval_mapping[col_idx]={}
				count_mapping[col_idx]=0

			# Filter operator specific logic, want to filter on -1 in older version and 1 in intified version
			if col_idx==10 and line_count==0 and split_line[col_idx]=='-1':
				count_mapping[col_idx]=1
			if split_line[col_idx] not in fieldval_mapping[col_idx]:
				fieldval_mapping[col_idx][split_line[col_idx]]=count_mapping[col_idx]
				count_mapping[col_idx]+=1
			split_line[col_idx]=fieldval_mapping[col_idx][split_line[col_idx]]
			if col_idx==0:
				fp_out.write(str(split_line[col_idx]))
			else:
				fp_out.write(","+str(split_line[col_idx]))
		fp_out.write("\n")
		line_count+=1
		line=fp.readline()

fp_out.close()

