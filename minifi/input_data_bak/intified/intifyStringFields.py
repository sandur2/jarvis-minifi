#!/usr/bin/py
import sys
import statistics

file_name=sys.argv[1]
column_num=int(sys.argv[2])

count=0
line_count=0

fieldval_mapping={}
out_file=file_name+".intify"
fp_out=open(out_file,"w")
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		split_line=line.split(",")
		# Filter operator specific logic, want to filter on -1 in older version and 1 in intified version
		if line_count==0 and column_num==10 and split_line[10]=='-1':
			count=1
		if split_line[column_num] not in fieldval_mapping:
			fieldval_mapping[split_line[column_num]]=count
			count+=1
		split_line[column_num]=fieldval_mapping[split_line[column_num]]
		fp_out.write(split_line[0])
		for col_idx in range(1,len(split_line)):
			fp_out.write(","+str(split_line[col_idx]))
		line_count+=1
		line=fp.readline()

fp_out.close()

