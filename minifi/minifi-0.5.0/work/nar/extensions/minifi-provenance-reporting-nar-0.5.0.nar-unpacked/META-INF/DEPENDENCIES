// ------------------------------------------------------------------
// Transitive dependencies of this project determined from the
// maven pom organized by organization.
// ------------------------------------------------------------------

minifi-provenance-reporting-nar


From: 'an unknown organization'
  - ParaNamer Core (http://paranamer.codehaus.org/paranamer) com.thoughtworks.paranamer:paranamer:bundle:2.7
    License: BSD  (LICENSE.txt)
  - Metrics Core Library (http://metrics.codahale.com/metrics-core/) com.yammer.metrics:metrics-core:jar:2.2.0
    License: Apache License 2.0  (http://www.apache.org/licenses/LICENSE-2.0.html)
  - XZ for Java (http://tukaani.org/xz/java.html) org.tukaani:xz:jar:1.5
    License: Public Domain 

From: 'Apache NiFi Project' (http://nifi.apache.org/)
  - nifi-api (http://nifi.apache.org/nifi-api) org.apache.nifi:nifi-api:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-avro-record-utils (http://nifi.apache.org/nifi-nar-bundles/nifi-extension-utils/nifi-record-utils/nifi-avro-record-utils) org.apache.nifi:nifi-avro-record-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-framework-api (http://nifi.apache.org/nifi-framework-api) org.apache.nifi:nifi-framework-api:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-processor-utils (http://nifi.apache.org/nifi-nar-bundles/nifi-extension-utils/nifi-processor-utils) org.apache.nifi:nifi-processor-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-properties (http://nifi.apache.org/nifi-commons/nifi-properties) org.apache.nifi:nifi-properties:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-record (http://nifi.apache.org/nifi-commons/nifi-record) org.apache.nifi:nifi-record:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-record-serialization-service-api (http://nifi.apache.org/nifi-nar-bundles/nifi-standard-services/nifi-record-serialization-service-api) org.apache.nifi:nifi-record-serialization-service-api:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-reporting-utils (http://nifi.apache.org/nifi-nar-bundles/nifi-extension-utils/nifi-reporting-utils) org.apache.nifi:nifi-reporting-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-schema-registry-service-api (http://nifi.apache.org/nifi-nar-bundles/nifi-standard-services/nifi-schema-registry-service-api) org.apache.nifi:nifi-schema-registry-service-api:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-security-utils (http://nifi.apache.org/nifi-commons/nifi-security-utils) org.apache.nifi:nifi-security-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-site-to-site-client (http://nifi.apache.org/nifi-commons/nifi-site-to-site-client) org.apache.nifi:nifi-site-to-site-client:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-site-to-site-reporting-task (http://nifi.apache.org/nifi-nar-bundles/nifi-site-to-site-reporting-bundle/nifi-site-to-site-reporting-task) org.apache.nifi:nifi-site-to-site-reporting-task:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-standard-record-utils (http://nifi.apache.org/nifi-nar-bundles/nifi-extension-utils/nifi-record-utils/nifi-standard-record-utils) org.apache.nifi:nifi-standard-record-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-standard-services-api-nar (http://nifi.apache.org/nifi-nar-bundles/nifi-standard-services/nifi-standard-services-api-nar) org.apache.nifi:nifi-standard-services-api-nar:nar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - nifi-utils (http://nifi.apache.org/nifi-commons/nifi-utils) org.apache.nifi:nifi-utils:jar:1.7.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

From: 'FasterXML' (http://fasterxml.com)
  - Jackson (http://jackson.codehaus.org) org.codehaus.jackson:jackson-core-asl:jar:1.9.13
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'FasterXML' (http://fasterxml.com/)
  - Jackson-annotations (http://github.com/FasterXML/jackson) com.fasterxml.jackson.core:jackson-annotations:bundle:2.9.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Jackson-core (https://github.com/FasterXML/jackson-core) com.fasterxml.jackson.core:jackson-core:bundle:2.9.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - jackson-databind (http://github.com/FasterXML/jackson) com.fasterxml.jackson.core:jackson-databind:bundle:2.9.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'Oracle' (http://www.oracle.com)
  - JSR 353 (JSON Processing) API (http://json-processing-spec.java.net) javax.json:javax.json-api:bundle:1.0
    License: Dual license consisting of the CDDL v1.1 and GPL v2  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)
  - JSR 353 (JSON Processing) Default Provider (http://jsonp.java.net) org.glassfish:javax.json:bundle:1.0.4
    License: Dual license consisting of the CDDL v1.1 and GPL v2  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)

From: 'The Apache Software Foundation' (http://www.apache.org/)
  - Apache Commons Codec (http://commons.apache.org/proper/commons-codec/) commons-codec:commons-codec:jar:1.10
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Avro (http://avro.apache.org) org.apache.avro:avro:bundle:1.8.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Commons Compress (http://commons.apache.org/proper/commons-compress/) org.apache.commons:commons-compress:jar:1.11
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Commons CSV (http://commons.apache.org/proper/commons-csv/) org.apache.commons:commons-csv:jar:1.4
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'xerial.org'
  - snappy-java (https://github.comm/xerial/snappy-java) org.xerial.snappy:snappy-java:bundle:1.1.1.3
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)




